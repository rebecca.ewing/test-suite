# specify which config file to use
CONFIG=config.yml

# specify a name for the dag file
DAG_NAME=test_suite

# specify a location for the conda env
PREFIX=$(HOME)/.conda/envs/test-suite

INJECTIONS=injections/fake-data_injections.xml.gz

# define commands to activate the conda env
CONDA_ACTIVATE=source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh && conda deactivate && conda activate $(PREFIX)

.PHONY: help
help: 
	@echo 'Commands for Test Suite setup:'
	@echo 'make env 	generate environment (env.sh) to source'
	@echo 'make dag 	create DAG for condor submission'

conda-env :
	source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh && conda env create -f env/environment.yml --prefix=$(PREFIX)
	@echo 'created conda environment'

.PHONY: dag
dag : $(DAG_NAME).dag

$(INJECTIONS) :
	@$(CONDA_ACTIVATE) && cd injections && make mdc-injections && cd ..

$(DAG_NAME).dag : $(INJECTIONS)
	@$(CONDA_ACTIVATE) && test-suite-workflow -c ${CONFIG} -n ${DAG_NAME} -p ${PREFIX} -v

.PHONY: launch
launch : $(DAG_NAME).dag
	@if [[ ! $$(stat -c %a influx_creds.sh | tail -c 2) == "0" ]]; then\
		echo "Fixing influx_creds.sh permissions" && chmod o-rwx influx_creds.sh;\
	fi
	@$(CONDA_ACTIVATE) && source ./influx_creds.sh && \
	condor_submit_dag -include_env HOME,PATH,INFLUX_* $(DAG_NAME).dag

# The env.sh target is for development only
X509_USER_PROXY=$(shell grep '^    X509_USER_PROXY:' config.yml | awk '{print $$2}')
X509_USER_CERT=$(shell grep '^    X509_USER_CERT:' config.yml | awk '{print $$2}')
X509_USER_KEY=$(shell grep '^    X509_USER_KEY:' config.yml | awk '{print $$2}')
env.sh :
	@if [ $(X509_USER_PROXY) ]; then\
		echo 'export X509_USER_PROXY=$(X509_USER_PROXY)' >> env.sh;\
	fi
	@if [ $(X509_USER_CERT) ]; then\
		echo -e 'unset X509_USER_PROXY\nexport X509_USER_CERT=$(X509_USER_CERT)\nexport X509_USER_KEY=$(X509_USER_KEY)' >> env.sh;\
	fi
	@echo 'export GSTLAL_FIR_WHITEN=0' >> env.sh
	@echo 'source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh' >> env.sh
	@echo 'conda activate $(PREFIX)' >> env.sh
	@echo '. influx_creds.sh' >> env.sh
	@echo 'created env.sh'
	@echo -e '\n\tWARNING\nUse "make launch" to submit the dag\nSourcing env.sh does not affect the dag\n'

## Quickstart

Each branch in this repository represents a configuration for a specific flavor of analysis, eg:
* main: fake data scheme, set up for running on CIT
* gstlal-allsky-icds
* gstlal-allsky-cit
* gstlal-ew-cit

Choose the appropriate branch for your analysis and clone the repo to a directory where you want to launch the Test Suite:

```
git clone -b <BRANCH> https://git.ligo.org/rebecca.ewing/test-suite.git
```

## Installation

The Makefile includes targets for installing `gw-lts` and all its dependencies in a conda env. You can optionally set a location for the conda env by editing the `PREFIX` variable in the Makefile, the default is `$(HOME)/.conda/envs/test-suite`.

Make the conda environment:
```bash
make conda-env
```

## Configuration and Credentials

### Influx credentials
In order to output metrics on Grafana, you will need credentials to access a unique Influx database. You will need to request access from `rebecca.ewing` (CIT and ICDS) or `ron.tapia` (CIT and ICDS) or `duncan.meacher` (NEMO) by sending in a Mattermost DM: 
* an Influx username and password
* unique name of an Influx database, for example: `test_suite_{yourname}`

Once you have these set up, you can proceed to the following.

```bash
cp influx_creds.sh.sample influx_creds.sh
chmod o-rwx influx_creds.sh
```

* In `influx_creds.sh` add your influx username and password.
* In `web/test-suite.yml` set `db` in the `backend` section (bottom of the file) to your Influx database name.

### SCiMMA credentials

If you want to use the Test Suite to analyze real search results from GraceDB (ie you are not using the fake data configuration), then you will need to set up hop credentials. Otherwise, you can skip this step.
See instructions here: https://computing.docs.ligo.org/igwn-alert/client/guide.html#managing-credentials-and-topics

**Note**: hop credentials are already set up on gstlalcbc shared accounts, so you can skip this step if setting up there.

### LIGO Proxy

If you are analyzing real data from GraceDB you will also need a valid ligo-proxy: `ligo-proxy-init albert.einstein`.

**Note**: this command is only necessary if you are creating your own proxy file. You do not need to run `ligo-proxy-init` from a gstlal shared account.

When editing the config.yml file in the next section, you will need the full path to your proxy or your shared account's robot certificate. The path to the currently active proxy can be found with: `ecp-cert-info --path`

### Config options

Now you will need to make some minimal changes to the configuration file, for example add a unique tag:
```yaml
tag: my_analysis_tag
```

Set your accounting group user name:
```yaml
condor:
  accounting_group_user: albert.einstein
```

If analyzing real data, add the path to either your ligo proxy, or the robot cert/key for your shared account:
```yaml
  environment:
    X509_USER_PROXY: /path/to/proxy
    #X509_USER_CERT: /path/to/cert
    #X509_USER_KEY: /path/to/cert
```

Make sure to check additional config options such as:
* `gracedb_server`
* `time-offset` to shift injections by (needed for O3 replay)
* `input topics` from which to consume state vector segments and PSDs (used to estimate injected SNRs and decisive SNRs)

## Set up

Generate the Condor DAG for submission: `make dag`

## Submit

Submit the DAG: `make launch`

## Visualize with Grafana 
Follow the instructions below to set up your dashboard on Grafana. 

1. Download the `web/dashboard.json` to your local machine.
2. Log in to https://gstlal.ligo.caltech.edu/grafana with your LIGO credentials.
3. Hover over the plus symbol on the left and click "Import".
4. Click on "Upload JSON file" and select your downloaded dashboard.json.
5. Change "Name" to a title for your dashboard and choose a unique UID to identify your dashboard (used in the dashboard URL).
6. Click Import
7. In the upper left of your dashboard, select your Influx db from the drop down menu next to Dashboard Datasource.

**Note**: If you do not have editor access to Grafana you can request it from `rebecca.ewing` or `ron.tapia`


